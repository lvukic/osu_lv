import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as Image
from sklearn.cluster import KMeans
from PIL import Image as im 

#import os
#print(os.getcwd())

# ucitaj sliku
img = Image.imread("imgs/test_1.jpg")
#print(img)
#print(len(img[0])) #1195 redova i 601 stupaca

# prikazi originalnu sliku
plt.figure()
plt.title("Originalna slika")
plt.imshow(img)
plt.tight_layout()
plt.show()

# pretvori vrijednosti elemenata slike u raspon 0 do 1
img = img.astype(np.float64) / 255

# transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
w, h, d = img.shape
img_array = np.reshape(img, (w*h, d))

# rezultatna slika
img_array_aprox = img_array.copy()

#1
print("Broj jedinstvenih boja je: " + str(len(np.unique(img_array.reshape(-1, img.shape[2]), axis=0))))

#2
km = KMeans(n_clusters=5, init='k-means++', n_init=10, random_state=0)
km.fit(img_array)
labels = km.predict(img_array)

#3
for i in range(0, len(labels)):
    img_array_aprox[i] = km.cluster_centers_[labels[i]]

img_aprox = np.reshape(img_array_aprox, (w, h, d))
img_aprox = (img_aprox*255).astype(np.uint8)
plt.figure()
#4
plt.title("KMeans slika")
plt.imshow(img_aprox)
plt.tight_layout()
plt.show()
#sto je k veci to je bolja kvaliteta slike ali je i manja kompresija

#6
inertias = []
K = range(1, 10)
mapping2 = {}
for k in K:
    kmeanModel = KMeans(n_clusters=k, init='random', n_init=5)
    kmeanModel.fit(img_array_aprox)
    colorLabels = kmeanModel.predict(img_array_aprox)
    inertias.append(kmeanModel.inertia_)

plt.plot(K, inertias, 'bx-')
plt.xlabel('Values of K')
plt.ylabel('Inertia')
plt.title('The Elbow Method using Inertia')
plt.show()
#najbolja k=2 ili k=5 jer poslje njega gotovo nema nikakve razlike

#7
un_labels = np.unique(labels)
for i in range(0, len(un_labels)):
    bin_img = labels == un_labels[i]
    bin_img = np.reshape(bin_img, (w, h))
    plt.figure()
    plt.title(f"Binary image's {i+1} centar")
    plt.imshow(bin_img)
    plt.show()
	
#kako se povecava k opada kvaliteta slike