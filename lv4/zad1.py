from sklearn import datasets
from sklearn . model_selection import train_test_split
from sklearn . preprocessing import MinMaxScaler
from sklearn . preprocessing import OneHotEncoder
import sklearn . linear_model as lm
from sklearn . metrics import mean_absolute_error, mean_squared_error, mean_absolute_percentage_error, r2_score
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from math import sqrt

#0
data = pd.read_csv('data_C02_emission.csv')
x =  data[["Engine Size (L)", 
           "Cylinders", 
           "Fuel Consumption City (L/100km)", 
           "Fuel Consumption Hwy (L/100km)",
           "Fuel Consumption Comb (L/100km)",
           "Fuel Consumption Comb (mpg)"]]

y = data[["CO2 Emissions (g/km)"]]

#a
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=1)

#b
plt.xlabel("Fuel Consumption City (L/100km)")
plt.ylabel("CO2 Emissions (g/km)")
plt.scatter(x_train["Fuel Consumption City (L/100km)"], y_train, color="blue")
plt.scatter(x_test["Fuel Consumption City (L/100km)"], y_test, color="red")
plt.show()

#c
sc = MinMaxScaler()
x_train_n = sc.fit_transform(x_train)
x_test_n = sc.transform(x_test)

plt.subplot(1,2,1)
plt.xlabel("Fuel Consumption City (L/100km)")
plt.ylabel("Number of measurment")
plt.hist(x_train["Fuel Consumption City (L/100km)"])
plt.title("Original data")

plt.subplot(1,2,2)
plt.xlabel("Fuel Consumption City (L/100km)")
plt.ylabel("Number of measurment")
plt.hist(x_train_n[:, 2])
plt.title("Standardize data")

plt.show()

#d
linearModel = lm.LinearRegression()
linearModel.fit( x_train_n , y_train)
print(linearModel.coef_)

#e
y_test_p = linearModel.predict(x_test_n)
plt.scatter(x_test_n[:, 2], y_test)
plt.scatter(x_test_n[:, 2], y_test_p)
plt.title("Real vs Predicted value")
plt.legend(["Real value", "Predicted value"])
plt.show()

#f
MSE = mean_squared_error(y_test, y_test_p)
RMSE = sqrt(MSE)
MAE = mean_absolute_error(y_test, y_test_p)
MAPE = mean_absolute_percentage_error(y_test, y_test_p)
R2 = r2_score(y_test, y_test_p)

print("MSE: " + str(MSE))
print("RMSE: " + str(RMSE))
print("MAE: " + str(MAE))
print("MAPE: " + str(MAPE))
print("R2: " + str(R2))

#g
#One postaju losije