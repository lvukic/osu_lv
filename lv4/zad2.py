from sklearn import datasets
from sklearn . model_selection import train_test_split
from sklearn . preprocessing import MinMaxScaler
from sklearn . preprocessing import OneHotEncoder
import sklearn . linear_model as lm
from sklearn . metrics import mean_absolute_error, mean_squared_error, mean_absolute_percentage_error, r2_score, max_error
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from math import sqrt

data = pd.read_csv('data_C02_emission.csv')
x =  data[["Engine Size (L)", 
           "Cylinders",
		   "Fuel Type",
           "Fuel Consumption City (L/100km)", 
           "Fuel Consumption Hwy (L/100km)",
           "Fuel Consumption Comb (L/100km)",
           "Fuel Consumption Comb (mpg)"]]

y = data[["CO2 Emissions (g/km)"]]

ohe = OneHotEncoder()
X_encoded = ohe.fit_transform(x[["Fuel Type"]])
x[ohe.categories_[0]] = X_encoded.toarray()
x = x.drop(['Fuel Type'], axis=1)

x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=1)

linearModel = lm.LinearRegression()
linearModel.fit( x_train , y_train)
print(linearModel.coef_)

y_test_p = linearModel.predict(x_test)

max_err = max_error(y_test, y_test_p)
print(max_err)
#max_error je 31.156