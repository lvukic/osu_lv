import numpy as np
import matplotlib.pyplot as plt

x_array = np.array([1, 3, 3, 2, 1])
y_array = np.array([1, 1, 2, 2, 1])
plt.plot(x_array, y_array, linewidth=1, marker="o", markersize=5)
plt.axis([0.0, 4.0, 0.0, 4.0])
plt.xlabel("x os")
plt.ylabel("y os")
plt.title("Primjer")
plt.show()