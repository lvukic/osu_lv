import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt("data.csv", delimiter=",", skiprows=1)
print("Mjerenje je izvršeno nad " + str(data.shape[0]) + " osoba")
x = data[:,1]
y = data[:,2]
plt.subplot(1, 2, 1)
plt.scatter(x, y)
plt.xlabel("Visina")
plt.ylabel("Masa")
plt.title("Omjer visine i mase")


x = x[0::50]
y = y[0::50]
plt.subplot(1, 2, 2)
plt.scatter(x, y)
plt.xlabel("Visina")
plt.ylabel("Masa")
plt.title("Omjer visine i mase")

plt.show()

x = data[:,1]
print("Min visina je: " + str(x.min()))
print("Max visina je: " + str(x.max()))
print("Srednja visina je: " + str(x.mean()))

male = data[data[:,0] == 1]
print("Min visina muškarca je: " + str(male[:,1].min()))
print("Max visina muškarca je: " + str(male[:,1].max()))
print("Srednja visina muškarca je: " + str(male[:,1].mean()))

female = data[data[:,0] == 0]
print("Min visina žene je: " + str(female[:,1].min()))
print("Max visina žene je: " + str(female[:,1].max()))
print("Srednja visina žene je: " + str(female[:,1].mean()))

#male = data[data[:,0] == 1]
#minn = male[:,1].min()
#print(minn)

"""
male = (data[:,0] == 1)
female = (data[:,0] == 0)
"""