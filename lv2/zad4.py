import numpy as np
import matplotlib.pyplot as plt

black = np.zeros((50,50))
white = np.full((50,50), 255)

img_t = np.hstack((black, white))
img_b = np.hstack((white, black))
img = np.vstack((img_t, img_b))

print(img.shape)
print(img.dtype)

plt.figure()
plt.imshow(img,cmap = "gray")
plt.show()