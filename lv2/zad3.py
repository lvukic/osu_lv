import numpy as np
import matplotlib.pyplot as plt

imgg = plt.imread("road.jpg")
img = imgg[:,:,0].copy()
print(img.shape)
print(img.dtype)
plt.figure()
plt.imshow(img,cmap = "gray")
plt.show()

#a prosvjetliti
img_b = imgg[:,:,0].copy()

for x in range(0, img_b.shape[0]):
	for y in range(0, img_b.shape[1]):
		if img_b[x,y] < 206:
			img_b[x,y] = img_b[x,y] + 50
		
plt.figure()
plt.imshow(img_b, cmap = "gray")
plt.show()

#b samo druga cetvrtina slike po sirini
lower_boundry = int(imgg.shape[1]/4)
upper_boundy = int(imgg.shape[1]/4*2)
img_c = imgg[:,lower_boundry:upper_boundy,0].copy()
plt.figure()
plt.imshow(img_c, cmap = "gray")
plt.show()

#c zarotirat sliku za 90 stupnjeva u smjeru kazaljke na satu
img_r = imgg[:,:,0].copy()
img_r = np.rot90(img_r, k=1, axes=(1,0))
plt.figure()
plt.imshow(img_r, cmap = "gray")
plt.show()

#d zrcaliti sliku
img_z = imgg[:,:,0].copy()
img_z = np.fliplr(img_z)
plt.figure()
plt.imshow(img_z, cmap = "gray")
plt.show()