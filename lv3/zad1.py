#a)
import pandas as pd

data = pd.read_csv('data_C02_emission.csv')

print(" ")
print("a)")

print(data.info())
#print(data.isnull()) #provjera praznih vrijednosti
data.dropna(axis=0, inplace = True) #brisanje izostajalih vrijednosti
#print(data.duplicated()) #provjera duplikata
data.drop_duplicates(inplace = True) #brisanje duplikata


for col in ["Make", "Model", "Vehicle Class", "Transmission", "Fuel Type"]:
    data[col] = data[col].astype('category')

#b)
print(" ")
print("b)")

largest = data.nlargest(3, "Fuel Consumption City (L/100km)")
print(largest[["Make", "Model", "Fuel Consumption City (L/100km)"]])

smallest = data.nsmallest(3, "Fuel Consumption City (L/100km)")
print(smallest[["Make", "Model", "Fuel Consumption City (L/100km)"]])

#c)
print(" ")
print("c)")

engine_size = data[(data["Engine Size (L)"] > 2.5) & (data["Engine Size (L)"] < 3.5)]
print(len(engine_size))
print(engine_size["CO2 Emissions (g/km)"].mean())

#d)
print(" ")
print("d)")

audi = data[data["Make"] == "Audi"]
print(len(audi))
audi_4 = audi[audi["Cylinders"] == 4]
print(audi_4["CO2 Emissions (g/km)"].mean())

#e)
print(" ")
print("e)")
grouped_cylinders = data.groupby(["Cylinders"]).size()
print(grouped_cylinders)

num_of_cilinders = data["Cylinders"].unique()
for x in num_of_cilinders:
	cylinders_mean = data[data["Cylinders"] == x]
	print(str(x) + ": " + str(cylinders_mean["CO2 Emissions (g/km)"].mean()))

#f)
print(" ")
print("f)")

dizel = data[data["Fuel Type"] == "D"]
print("dizel average: " + str(dizel["Fuel Consumption City (L/100km)"].mean()))
print("dizel median: " + str(dizel["Fuel Consumption City (L/100km)"].median()))

benzin = data[data["Fuel Type"] == "X"]
print("benzin average: " + str(benzin["Fuel Consumption City (L/100km)"].mean()))
print("benzin median: " + str(benzin["Fuel Consumption City (L/100km)"].median()))

#g)
print(" ")
print("g)")

max_4 = data[data["Fuel Type"] == "D"]
max_4 = max_4[data["Cylinders"] == 4]
print(max_4["Fuel Consumption City (L/100km)"].max())

#h)
print(" ")
print("h)")

print(data[data["Transmission"].str.contains("AM")].count())

#i)
print(" ")
print("i)")

print(data.corr())

#vidimo da velicina motora, broj cilindara, co2 emisije, potrosnja goriva imaju veliku korelaciju sto znaci da 
#povecanjem jedne varijable i druga ce se varijabla povecati i to znacajno