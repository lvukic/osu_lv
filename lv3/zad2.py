import pandas as pd
import matplotlib.pyplot as plt

data = pd.read_csv('data_C02_emission.csv')

print(data.info())
#print(data.isnull()) #provjera praznih vrijednosti
data.dropna(axis=0, inplace = True) #brisanje izostajalih vrijednosti
#print(data.duplicated()) #provjera duplikata
data.drop_duplicates(inplace = True) #brisanje duplikata


for col in ["Make", "Model", "Vehicle Class", "Transmission", "Fuel Type"]:
    data[col] = data[col].astype('category')
	
#a
print(" ")
print("a)")

his = data["CO2 Emissions (g/km)"]
plt.hist(his)
plt.show()

#vidimo da distribucija dobro prati normalnu distribuciju s nekoliko izuzetaka koji imaju znacajno veci potrosnju
#u odnosu na prosijek

#b 
print(" ")
print("b)")

city = data["Fuel Consumption City (L/100km)"]
co2 = data["CO2 Emissions (g/km)"]

fuel = data["Fuel Type"].map({"D":0, "E":1, "X":2, "Z":3})

plt.scatter(data["Fuel Consumption City (L/100km)"], data["CO2 Emissions (g/km)"], c=fuel, cmap="bwr")
plt.show()

#vidimo da vrijednosi poprilicno dobro koreliraju te da porastom jedne vrijednosti raste i druga vrijednost

#c 
print(" ")
print("c)")

consumption = data[["Fuel Consumption Hwy (L/100km)","Fuel Type"]]
data1 = consumption[consumption["Fuel Type"] == "D"]
data1 = data1["Fuel Consumption Hwy (L/100km)"]
data2 = consumption[consumption["Fuel Type"] == "E"]
data2 = data2["Fuel Consumption Hwy (L/100km)"]
data3 = consumption[consumption["Fuel Type"] == "X"]
data3 = data3["Fuel Consumption Hwy (L/100km)"]
data4 = consumption[consumption["Fuel Type"] == "Z"]
data4 = data4["Fuel Consumption Hwy (L/100km)"]

combine = pd.DataFrame({"D":data1, "E":data2, "X":data3, "Z":data4})
ax = combine[['D', 'E', 'X', 'Z']].plot(kind='box', title='Potrosnja van grada')
plt.show()

#vidimo da je distribucija jako uska oko D vrste goriva s nekoliko strsecih vrijednosti dok pak za Z imamo brojne strsece vrijednosti

#d 
print(" ")
print("d)")

stupcasti = data.groupby(["Fuel Type"]).size()
label = ["D", "E", "X", "Z"]

#plt.bar(label, stupcasti)
#plt.show()

#e
print(" ")
print("e)")

num_of_cilinders = data["Cylinders"].unique()
co2avg = []
for x in num_of_cilinders:
	cylinders_mean = data[data["Cylinders"] == x]
	co2avg.append(cylinders_mean["CO2 Emissions (g/km)"].mean())
	
#plt.bar(num_of_cilinders, co2avg)
fig, axs = plt.subplots(2)
axs[0].bar(label, stupcasti)
axs[1].bar(num_of_cilinders, co2avg)
plt.show()