import numpy as np
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.datasets import cifar10
from tensorflow.keras.utils import to_categorical
from matplotlib import pyplot as plt


# ucitaj CIFAR-10 podatkovni skup
(X_train, y_train), (X_test, y_test) = cifar10.load_data()

# prikazi 9 slika iz skupa za ucenje
plt.figure()
for i in range(9):
    plt.subplot(330 + 1 + i)
    plt.xticks([]),plt.yticks([])
    plt.imshow(X_train[i])

plt.show()


# pripremi podatke (skaliraj ih na raspon [0,1]])
X_train_n = X_train.astype('float32')/ 255.0
X_test_n = X_test.astype('float32')/ 255.0

# 1-od-K kodiranje
y_train = to_categorical(y_train)
y_test = to_categorical(y_test)
y_train = to_categorical(y_train, dtype ="uint8")
y_test = to_categorical(y_test, dtype ="uint8")

# CNN mreza
model = keras.Sequential()
model.add(layers.Input(shape=(32,32,3)))
model.add(layers.Conv2D(filters=32, kernel_size=(3, 3), activation='relu', padding='same'))
model.add(layers.MaxPooling2D(pool_size=(2, 2)))
model.add(layers.Conv2D(filters=64, kernel_size=(3, 3), activation='relu', padding='same'))
model.add(layers.MaxPooling2D(pool_size=(2, 2)))
model.add(layers.Conv2D(filters=128, kernel_size=(3, 3), activation='relu', padding='same'))
model.add(layers.MaxPooling2D(pool_size=(2, 2)))
model.add(layers.Flatten())
model.add(layers.Dense(500, activation='relu'))
model.add(layers.Dropout(0.5))
model.add(layers.Dense(10, activation='softmax'))

model.summary()

# definiraj listu s funkcijama povratnog poziva
my_callbacks = [
	keras.callbacks.EarlyStopping(monitor = "val_loss",
								  patience = 5,
								  verbose = 1),

    keras.callbacks.TensorBoard(log_dir = 'logs/cnn',
                                update_freq = 100)
]

model.compile(optimizer='adam',
                loss='categorical_crossentropy',
                metrics=['accuracy'])

model.fit(X_train_n,
            y_train,
            epochs = 40,
            batch_size = 64,
            callbacks = my_callbacks,
            validation_split = 0.1)


score = model.evaluate(X_test_n, y_test, verbose=0)
print(f'Tocnost na testnom skupu podataka: {100.0*score[1]:.2f}')

#1.1
#Ulazni sloj je 32x32 piksela s tri dimenzije koje predstavljaju boju
#poslje toga ide naizmjenicno sloj 2dkonvolucije 3x3 dimenzije pa
#MaxPolling2D koji je 2x2
#na kraju imamo ravnanje sloja u izlaz koji ce predstavljati niz 
#skriveni sloj ima 500 neurona s aktivacijskom funkcijom relu
#izlazni sloj ima 10 neurona a aktivacijskom funkcijom softmax
#Svi konvolucijski slojevi se aktiviraju na relu funkciju

#1.2
#model ima 1,122,758 parametara

#1.3
#na pocetku vidimo tocnost klasifikacije i funkciju gubitka skupova za
#treniranje i validaciju
#na pocetku tocnost raste dok gubitak opada za oba skupa
#nakon određenog vremena validacija doseze maksimalnih 75% tocnosti
#nakon dosegnutog maksimuma samo tocnost skupa za treniranje raste
#u tom trenutku za trening skup gubitak i dalje pada ali za validacijski
#skup on pocinje rasti

#2
#dropout sloj je nesto poboljsao performance modela jer je sprijecio 
#pretreniranje 
