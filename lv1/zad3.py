from statistics import mean

def is_float(string):
    try:
        float(string)
        return True
    except ValueError:
        return False

my_num_list = []
print("Input numbers which we add into the list and when you done write Done: ")
while(True):
    user_input = input()
    if user_input == "Done":
        break
    elif is_float(user_input):
        my_num_list.append(float(user_input))
    else:
        print("Input is not number, try again...")

my_num_list.sort()

print("Length of list is: " + str(len(my_num_list)))
print("List mean is: " + str(mean(my_num_list)))
print("List min is: " + str(my_num_list[0]))
print("List max is: " + str(my_num_list[-1]))
print("Sorted list is: " + str(my_num_list))