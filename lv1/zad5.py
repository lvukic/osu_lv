ham = 0
spam = 0

spam_with_exclamation = 0

ham_word = 0
spam_word = 0

fhand = open("C:\\Users\\student\\Desktop\\lv1osu\\osu_lv\\lv1\\SMSSpamCollection.txt")
for line in fhand:
    line = line.rstrip()
    #print(line)
    words = line.split()
    if words[0] == "ham":
        ham = ham + 1
        ham_word = ham_word + len(words)
    elif words[0] == "spam":
        spam = spam + 1
        spam_word = spam_word + len(words)
        if '!' == words[-1][-1]:
            spam_with_exclamation = spam_with_exclamation + 1

fhand.close()
print("Average percent of ham messages is: " + str(ham_word/ham))
print("Average percent of spam messages is: " + str(spam_word/spam))
print("Number of spam messages with exclamation mark is: " + str(spam_with_exclamation))
