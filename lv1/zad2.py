x = input("Input number: ")

try:
    x = float(x)
    if x > 1 or x < 0:
        print("Number is not in interval between 0.0 and 1.0")
    elif x >= 0.9:
        print("A")
    elif x >= 0.8:
        print("B")
    elif x >= 0.7:
        print("C")
    elif x >= 0.6:
        print("D")
    else:
        print("F")

except:
    print("Input is not number")